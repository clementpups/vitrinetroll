import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonList,
    IonCard,
    IonCardHeader,
    IonCardSubtitle, IonCardTitle, IonCardContent
} from '@ionic/react';
import React from 'react';
import './Home.css';

const Home: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Jacket off market</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    <IonCard>
                        <img src="assets/icon/k3_nice.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.99€</IonCardSubtitle>
                            <IonCardTitle>Krrish 3</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/trool2.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.09€</IonCardSubtitle>
                            <IonCardTitle>Trool 2</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/Conan_le_barbare.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.00€</IonCardSubtitle>
                            <IonCardTitle>Conan le Barbare</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/juan_skysharks700.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.49€</IonCardSubtitle>
                            <IonCardTitle>Sky Shark</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/kungFuKarate.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.99€</IonCardSubtitle>
                            <IonCardTitle>Justicia Justiciera 3</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/maxresdefault.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.99€</IonCardSubtitle>
                            <IonCardTitle>Drunken Master</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/Rape_Zombie_Lust_of_the_Dead_3.png" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.99€</IonCardSubtitle>
                            <IonCardTitle>Rape Zombie 3</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/Wolfcop.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>19.99€</IonCardSubtitle>
                            <IonCardTitle>Wolfcop</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/81xank2Z9CL._RI_.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>49.99€</IonCardSubtitle>
                            <IonCardTitle>Samurai Cop</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/86bb4e2e18a3b9a2af9730dc676369e0.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>39.99€</IonCardSubtitle>
                            <IonCardTitle>Blood Sport</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/Affiche_Cinema_Hitman.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>29.99€</IonCardSubtitle>
                            <IonCardTitle>Hitman le Cobra</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/DM2267.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>19.99€</IonCardSubtitle>
                            <IonCardTitle>With age comes wisdom and saggy man boobs</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                    <IonCard>
                        <img src="assets/icon/unnamed.jpg" alt="./icon.png"/>
                        <IonCardHeader>
                            <IonCardSubtitle>9.99€</IonCardSubtitle>
                            <IonCardTitle>Birdemic</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            Jacket en papier. 50cm * 60cm
                        </IonCardContent>
                    </IonCard>
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default Home;
